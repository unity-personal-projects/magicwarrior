﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Gun : MonoBehaviour
{
    [SerializeField] private GameObject m_BulletPrefab = null;
    [SerializeField] private GameObject m_SpawnPoint = null;
    [SerializeField] private float m_BulletSpeed = 5f;
    [SerializeField] private float m_BulletLifespan = 10f;
    [SerializeField] private float m_Cooldown = .2f;
    private Coroutine m_GunshotCoroutine;

    private void Update()
    {
        if (SteamVR_Actions._default.GrabPinch.GetStateDown(SteamVR_Input_Sources.RightHand) && m_GunshotCoroutine == null)
        {
            m_GunshotCoroutine = StartCoroutine(Gunshot());
        }
    }
    
    private IEnumerator Gunshot()
    {
        GameObject g = Instantiate(m_BulletPrefab);
        g.transform.SetParent(m_SpawnPoint.transform);
        g.transform.localPosition = Vector3.zero;
        g.transform.localRotation = Quaternion.identity;
        g.transform.SetParent(null);
        g.GetComponent<Rigidbody>().velocity = g.transform.forward * m_BulletSpeed;
        yield return new WaitForSeconds(m_Cooldown);
        m_GunshotCoroutine = null;
        yield return new WaitForSeconds(m_BulletLifespan);
        Destroy(g);
    }
}
