﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutantManager : MonoBehaviour
{
    private Transform[] m_SpawnPoints = null;
    [SerializeField] private GameObject m_MutantPrefab = null;
    private bool isPlaying = true;
    private Transform m_Player = null;

    private void Awake()
    {
        m_SpawnPoints = gameObject.GetComponentsInChildren<Transform>();
        m_Player = GameObject.FindObjectOfType<Camera>().transform;
    }

    private IEnumerator Start()
    {
        while (isPlaying)
        {
            if (CountMonsters() < 10)
            {
                GameObject g = Instantiate(m_MutantPrefab);
                int spawnPointMutant = Random.Range(1, m_SpawnPoints.Length);
                g.transform.position = m_SpawnPoints[spawnPointMutant].position;
            }
            float nextSpawnCooldown = Random.Range(1f, 5f);
            yield return new WaitForSeconds(nextSpawnCooldown);
        }
    }
    private int CountMonsters()
    {
        return GameObject.FindObjectsOfType<Mutant>().Length;
    }
}
