﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using SDD.Events;

public class Mutant : MonoBehaviour
{
    [SerializeField] private float m_AttackCooldown = 5;
    private Animator m_Animator = null;
    private NavMeshAgent m_NavMeshAgent = null;
    private Transform m_Player = null;
    private Coroutine m_Bahavior = null;

    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_Player = GameObject.FindObjectOfType<Camera>().transform;
    }

    private void Start()
    {
        m_Bahavior = StartCoroutine(StartBehavior());
    }

    private IEnumerator StartBehavior()
    {
        Debug.Log(Vector3.Distance(transform.position, m_Player.position));
        while (true)
        {
            while (Vector3.Distance(transform.position, m_Player.position) > 2)
            {
                m_NavMeshAgent.SetDestination(m_Player.position);
                m_Animator.Play("Walking");
                yield return null;
            }
            m_NavMeshAgent.SetDestination(transform.position);
            while (Vector3.Distance(transform.position, m_Player.position) <= 2)
            {
                m_Animator.Play("Attack");
                yield return null;
                float length = m_Animator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
                yield return new WaitForSeconds(length / 2);
                EventManager.Instance.Raise(new MonsterTouchPlayerEvent());
                yield return new WaitForSeconds(length / 2);

                m_Animator.Play("Idle");
                yield return null;
                float idleLength = m_Animator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
                yield return new WaitForSeconds(idleLength + m_AttackCooldown);
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.GetComponent<Bullet>())
        {
            ParticleSystem[] trailsObjects = other.collider.gameObject.GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem ps in trailsObjects)
            {
                ps.transform.SetParent(null);
                var em = ps.emission;
                em.enabled = false;
            }
            Destroy(other.collider.gameObject);
            StopCoroutine(m_Bahavior);
            StartCoroutine(IsDying(trailsObjects));
        }
    }
    
    private IEnumerator IsDying(ParticleSystem[] trailsObjects)
    {
        GetComponent<Collider>().enabled = false;
        m_NavMeshAgent.SetDestination(transform.position);
        m_Animator.Play("Dying");
        yield return null;
        float length = m_Animator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
        yield return new WaitForSeconds(length);
        Vector3 startPos = transform.position;
        Vector3 endPos = transform.position - Vector3.up;
        float actualTime = 0;
        while (actualTime < 1)
        {
            actualTime += Time.deltaTime;
            transform.position = Vector3.Lerp(startPos, endPos, actualTime / 1);
            yield return null;
        }
        foreach (ParticleSystem sys in trailsObjects)
        {
            Destroy(sys.gameObject);
        }
        Destroy(gameObject);
    }
}
