﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicLoopManager : MonoBehaviour
{
    [SerializeField] private AudioClip[] m_MusicLoops = null;
    private AudioSource m_AudioSource = null;

    private void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    private IEnumerator Start()
    {
        while (m_MusicLoops.Length > 0)
        {
            for (int i = 0; i < m_MusicLoops.Length; i++)
            {
                m_AudioSource.clip = m_MusicLoops[i];
                m_AudioSource.Play();
                yield return new WaitForSeconds(m_MusicLoops[i].length);
            }
        }
    }
}
